import subprocess

# Get latest coordinates from dud-poll, only for yes and maybe entries
oneliner = """curl -s https://dud-poll.inf.tu-dresden.de/jhv24/ | grep -oP 'td class="(vote a_yes__|vote b_maybe)" title="[^"]+"' | grep -P '[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?),\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)' | tee /dev/stderr | grep -oP '[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?),\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)' | sort -u"""
result = subprocess.run(oneliner, stdout=subprocess.PIPE, text=True, shell=True)
coordinates = []
for line in result.stdout.splitlines():
  values = line.split(', ')
  float_values = [float(value) for value in values]
  coordinate = tuple(float_values)
  coordinates.append(coordinate)

# Germany is small, let's just average lat/lon...
lat = []
lon = []

for l in coordinates:
  lat.append(l[0])
  lon.append(l[1])

midpoint = str(sum(lat)/len(lat)) + "," + str(sum(lon)/len(lon))

markers = ""
for l in coordinates:
  markers += f'&marker={l[0]},{l[1]}'

print(f'https://data.p173.de/midpoint/?center={midpoint}&zoom=6{markers}&specialMarker={midpoint}')
